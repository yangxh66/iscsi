# scst

## 源码编译 [SCST-project](https://github.com/SCST-project/scst.git)
```
[root@vm1 scst-3.6]# cd scst-3.6
[root@vm1 scst-3.6]# make 2release
[root@vm1 scst-3.6]# BUILD_2X_MODULE=y make all install
```
安装驱动
```
[root@vm1 scst-3.6]# for i in scst scst_vdisk scst_user ; do modprobe $i ; done
```
## 修改配置文件
```
[root@vm1 scst-3.6]# ln -s /usr/src/packages/BUILD/scst-3.6.0/iscsi-scst/etc/scst.conf /etc/
[root@vm1 scst-3.6]# vim /etc/scst.conf
        HANDLER vdisk_fileio {
                DEVICE disk01 {
                        filename /dev/sdc
                }
        }

        TARGET_DRIVER iscsi {
                enabled 1

                TARGET iqn.2022-05.net.vm1:scst {
                        LUN 0 disk01

                        enabled 1
                }
        }
 ```       
## 启动服务
```
[root@vm1 scst-3.6]# /usr/local/sbin/iscsi-scstd
[root@vm1 scst-3.6]# /etc/init.d/scst start
```
If you want SCST to start automatically at boot time, run the following command:
systemctl enable scst.service


