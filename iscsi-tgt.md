# iscsi-tgt

## 查询安装包
```
   [root@vm1 ~]# yum whatprovides iscsiadm
   Last metadata expiration check: 0:00:54 ago on Thu 12 May 2022 03:05:27 PM CST.
   open-iscsi-2.0.876-18.p01.ky10.aarch64 : ISCSI software initiator daemon and utility programs
   Repo        : kylinv10
   Matched from:
   Filename    : /usr/sbin/iscsiadm

   open-iscsi-2.0.876-19.ky10.aarch64 : ISCSI software initiator daemon and utility programs
   Repo        : @System
   Matched from:
   Filename    : /usr/sbin/iscsiadm
   [root@vm1 ~]#
```
```
   [root@vm1 ~]# yum whatprovides tgtd
   Last metadata expiration check: 0:06:01 ago on Thu 12 May 2022 03:05:27 PM CST.
   scsi-target-utils-1.0.79-1.ky10.aarch64 : The SCSI target daemon and utility programs
   Repo        : kylinv10
   Matched from:
   Filename    : /usr/sbin/tgtd

   scsi-target-utils-1.0.79-2.ky10.aarch64 : The SCSI target daemon and utility programs
   Repo        : @System
   Matched from:
   Filename    : /usr/sbin/tgtd
```
## 启动tgtd服务
   #查询tgtd服务状态
```
   [root@vm1 ~]# systemctl status tgtd
   ● tgtd.service - tgtd iSCSI target daemon
      Loaded: loaded (/usr/lib/systemd/system/tgtd.service; disabled; vendor preset: disabled)
      Active: inactive (dead)
   [root@vm1 ~]#
```
   #启动tgtd服务
```
   [root@vm1 ~]# systemctl start tgtd
   [root@vm1 ~]#
```
```
   [root@vm1 ~]# systemctl status tgtd
   ● tgtd.service - tgtd iSCSI target daemon
      Loaded: loaded (/usr/lib/systemd/system/tgtd.service; disabled; vendor preset: disabled)
      Active: active (running) since Thu 2022-05-12 16:07:59 CST; 6s ago
   Process: 80685 ExecStartPost=/bin/sleep 5 (code=exited, status=0/SUCCESS)
   Process: 80812 ExecStartPost=/usr/sbin/tgtadm --op update --mode sys --name State -v offline (code=exited, status=0/SUCCESS)
   Process: 80813 ExecStartPost=/usr/sbin/tgt-admin -e -c $TGTD_CONFIG (code=exited, status=0/SUCCESS)
   Process: 80818 ExecStartPost=/usr/sbin/tgtadm --op update --mode sys --name State -v ready (code=exited, status=0/SUCCESS)
   Main PID: 80684 (tgtd)
      Tasks: 2
      Memory: 9.5M
      CGroup: /system.slice/tgtd.service
            └─80684 /usr/sbin/tgtd -f

   May 12 16:07:54 vm1 systemd[1]: Starting tgtd iSCSI target daemon...
   May 12 16:07:54 vm1 tgtd[80684]: tgtd: iser_ib_init(3431) Failed to initialize RDMA; load kernel modules?
   May 12 16:07:54 vm1 tgtd[80684]: tgtd: work_timer_start(146) use timer_fd based scheduler
   May 12 16:07:54 vm1 tgtd[80684]: tgtd: bs_init(393) use pthread notification
   May 12 16:07:59 vm1 systemd[1]: Started tgtd iSCSI target daemon.
```
   查看tgtd使用的端口
```
   [root@vm1 ~]# netstat -nltp |grep tgtd
   tcp        0      0 0.0.0.0:3260            0.0.0.0:*               LISTEN      80684/tgtd
   tcp6       0      0 :::3260                 :::*                    LISTEN      80684/tgtd
```
## 创建target
```
   #创建target
   [root@vm1 ~]# tgtadm --lld iscsi --mode target --op new --tid 1 --targetname iqn.2022-05.com.vm1:for.all

   #查询是否创建成功
   [root@vm1 ~]# tgtadm --lld iscsi --op show --mode target
   Target 1: iqn.2022-05.com.vm1:for.all
      System information:
         Driver: iscsi
         State: ready
      I_T nexus information:
      LUN information:
         LUN: 0
               Type: controller
               SCSI ID: IET     00010000
               SCSI SN: beaf10
               Size: 0 MB, Block size: 1
               Online: Yes
               Removable media: No
               Prevent removal: No
               Readonly: No
               SWP: No
               Thin-provisioning: No
               Backing store type: null
               Backing store path: None
               Backing store flags:
      Account information:
      ACL information:
   [root@vm1 ~]#
```
## 为target添加逻辑单元
```
   #添加设备到target
   [root@vm1 ~]# tgtadm --lld iscsi --op new --mode logicalunit --tid 1 --lun 1 -b /dev/sdb
   [root@vm1 ~]#
   #查询信息
   [root@vm1 ~]# tgtadm --lld iscsi --op show --mode target
   Target 1: iqn.2022-05.com.vm1:for.all
      System information:
         Driver: iscsi
         State: ready
      I_T nexus information:
      LUN information:
         LUN: 0
               Type: controller
               SCSI ID: IET     00010000
               SCSI SN: beaf10
               Size: 0 MB, Block size: 1
               Online: Yes
               Removable media: No
               Prevent removal: No
               Readonly: No
               SWP: No
               Thin-provisioning: No
               Backing store type: null
               Backing store path: None
               Backing store flags:
         LUN: 1
               Type: disk
               SCSI ID: IET     00010001
               SCSI SN: beaf11
               Size: 20401 MB, Block size: 512
               Online: Yes
               Removable media: No
               Prevent removal: No
               Readonly: No
               SWP: No
               Thin-provisioning: No
               Backing store type: rdwr
               Backing store path: /dev/sdb
               Backing store flags:
      Account information:
      ACL information:
   [root@vm1 ~]#
```
  #将target信息保存到配置文件
```
   [root@vm1 ~]# tgt-admin --dump > /etc/tgt/targets.conf
   [root@vm1 ~]#
```
## 客户端访问target
  #连接target
```
   [root@vm1 ~]# iscsiadm -m discovery -t st -p 10.0.0.118
   10.0.0.118:3260,1 iqn.2022-05.com.vm1:for.all
   [root@vm1 ~]#
```
  #登录target
```
   [root@vm1 ~]# iscsiadm -m node --login
   Logging in to [iface: default, target: iqn.2022-05.com.vm1:for.all, portal: 10.0.0.118,3260]
   Login to [iface: default, target: iqn.2022-05.com.vm1:for.all, portal: 10.0.0.118,3260] successful.
   [root@vm1 ~]#
```
  #查看映射的设备名
```
   [root@vm1 ~]# ls -l /dev/disk/by-path/
   total 0
   lrwxrwxrwx 1 root root  9 May 12 18:02 ip-10.0.0.118:3260-iscsi-iqn.2022-05.com.vm1:for.all-lun-1 -> ../../sde
```



